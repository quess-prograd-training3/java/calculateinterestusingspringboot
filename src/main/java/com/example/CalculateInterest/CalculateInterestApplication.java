package com.example.CalculateInterest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class CalculateInterestApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext context=SpringApplication.run(CalculateInterestApplication.class, args);
		InterestCalculator interestCalculator=context.getBean(InterestCalculator.class);
		interestCalculator.intializeAge();
		System.out.println(interestCalculator.FindInterest(20000));
	}

}
