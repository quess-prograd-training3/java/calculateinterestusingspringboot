package com.example.CalculateInterest;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class InterestCalculator {
    private int age;

    public InterestCalculator() {
        //this.age=60;
    }
    public void intializeAge(){
        Scanner scanner=new Scanner(System.in);
        age=scanner.nextInt();
    }

    public InterestCalculator(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public double FindInterest(double amount){
        if(age >= 60){
            return (amount)*8/100;
        }
        else{
            return (amount)*4/100;
        }
    }
}
